<?php
$style_gallery_title = 'Simple Gallery';
$style_navigation_bullet = ' <span style="font-size: 10px;font-family:verdana;">-&gt;</span> ';
$style_name_slice = 20; // cut long names to 30 characters

$style_albums_start = '<table style="width:100%" cellspacing="0" cellpadding="5">';
$style_albums_rows = '<tr>';
$style_albums_cols = '<td style="text-align: center; background-color: #FAFAFA">';
$style_albums_columns = 4;

$style_images_start = '<table style="width:100%" cellspacing="1" cellpadding="8">';
$style_images_rows = '<tr>';
$style_images_cols = '<td style="text-align: center; background-color: #FAFAFA">';
$style_images_columns = 4;


$style_gallery = <<<EOT
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="Pragma" content="no-cache" />
<title>$style_gallery_title</title>
<style type="text/css">
body { font-family: tahoma, verdana, arial; font-size: 11px;}
a:link {color: #0066CC; font-size: 11px;}
a:hover {color: #FF6600;}
a:visited { color: #003366;}
a:active {color: #9DCC00;}
a img { border: 0px; }
span.album_description { font-size: 0.9em; display: block; color: #404040; }
span.image_description { font-size: 0.9em; display: block; color: #404040; }
#navigation { padding: 6px; border: 1px solid #E0E0E0; margin-bottom: 1px; }
#navigation a { color: #0066CC; }
#albums {padding: 5px; border: 1px solid #E0E0E0; font-size: 10px; line-height: 15px; margin-top: 5px;}
#albums a { font-size: 11px;}
#images {padding: 5px; border: 1px solid #E0E0E0; font-size: 10px; line-height: 15px; margin-top: 5px;}
#images a { font-size: 11px;}
#description {padding: 5px; background-color: #FAFAFA; line-height: 15px; margin-top: 5px;}
#pages { padding: 5px; margin-bottom: 1px; text-align: right;}
#sort {padding: 3px; line-height: 15px; margin-top: 5px; text-align: right}
#sort a:visited { color: #0066CC;}
#sort a:hover { color: #FF6600;}
#credit { padding: 5px; margin-bottom: 1px; text-align: center }

#previous {line-height: 16px; font-size: 10px; text-align: center;}
#current { padding: 10px; vertical-align: top; text-align: center; line-height: 15px; border: 1px #CCCCCC solid; background-color:#FAFAFA}
#next {line-height: 16px; font-size: 10px; text-align: center;}

/* border */
#next image { border: 1px black solid;}
#previous image { border: 1px black solid;}
#current image { border: 1px black solid;}
#albums image { border: 0px;}
#images image { border: 1px black solid;}
</style>
</head>
<body>
<div style="width: 600px; margin-left: auto; margin-right: auto;">
  <div id="navigation">{navigation}</div>
  {if browse_albums}
    <div id="sort">Sorting: {name_sort} {modified_sort} {size_sort} {album_images_sort} {album_albums_sort}</div>
    {if there_is_album_description}<div id="description">Album description: {show_album_description}</div>{endif there_is_album_description}
    {if there_are_albums}<div id="albums">{show_albums}</div>{endif there_are_albums}
    {if there_are_images}<div id="images">{show_images}</div>{endif there_are_images}
    {if there_are_pages}<div id="pages">{show_pages}</div>{endif there_are_pages}
  {endif browse_albums}
  {if view_image}
  <table style="width: 100%; margin-top: 5px; " cellspacing="5" cellpadding="0" >
    <tr>
     {if there_is_a_previous_image}<td style="text-align: left; vertical-align: top"><div id="previous">{show_previous}</div></td>{endif there_is_a_previous_image}
     {if current_image_exists}<td><div id="current">{show_current}</div></td>{endif current_image_exists}
     {if there_is_a_next_image}<td style="text-align: right; vertical-align: top"><div id="next">{show_next}</div></td>{endif there_is_a_next_image}
    </tr>
  </table>
  {endif view_image}
  <div id="credit"><!-- Please leave this -->Simple gallery, version 2 by <a href="http://celerondude.com">CeleronDude</a></div>
</div>
</body>
</html>
EOT;
?>
