A simple gallery script, version 2.2 (2) www.celerondude.com

How to install:
_____________________________________________________
Extract the files to a temporary directory. Upload them to your server. Keep the directory
structure intact. This is that everything should look like.

albums <- directory where you drop your albums into
cache <- directory where file lists are cached into, clear this everything you update your albums
images <- images and icons used by the gallery script
thumbnails <- the script looks in here for thumbnails
gallery.php <-- you include this into any pages, index.php for example.
index.php <- a sample file that shows you how you can just include gallery.php directly into your homepage
style_new.php <- simple styles, edit this to change the looks of the gallery
style_old.php <- old style from v2.1
style_simple.php <- basic html layout used if you're including the gallery into your existing layout.
functions.php <- functions used by the gallery

CHMOD "cache", "thumbnails" to writeable (0666 or 0777 or 0755). The script will tell you if the directories
don't have write permission.

If you have kept everything as default, the gallery should work right out of the box. Just run
index.php.

If you want to edit some settings, open gallery.php with your favorite text editor and edit inside the
configuration area. Marked like this

#----------------
# configurations
#----------------


How to add albums:
__________________________________
Albums are nothing more than directories with images in them. Simply
create directories inside the "albums" directory and CLEAR THE CACHE DIRECTORY. You can have
sub albums by creating sub directories.

How to comment albums:
___________________________________
Simply drop a text file named "description.txt" into any directory and the script will
display it.

