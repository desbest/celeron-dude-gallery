<?php
$style_gallery_title = 'Simple Gallery';
$style_navigation_bullet = ' <span style="font-size: 10px;font-family:verdana;">/</span> ';
$style_name_slice = 25; // cut long names to 30 characters

$style_albums_start = '<table id="albums_tbl" cellspacing="1" cellpadding="8">';
$style_albums_rows = '<tr>';
$style_albums_cols = '<td style="width:25%">';
$style_albums_columns = 4;

$style_images_start = '<table id="images_tbl" cellspacing="1" cellpadding="8">';
$style_images_rows = '<tr>';
$style_images_cols = '<td style="width:25%">';
$style_images_columns = 4;


$style_gallery = <<<EOT
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="Pragma" content="no-cache" />
<title>$style_gallery_title</title>
<style type="text/css">
body { font-family: tahoma, verdana, arial; font-size: 0.7em; background-color:#F0F0F0; cursor:default;}
a:link    { color: #0066CC; }
a:visited { color: #003366; }
a:hover   { text-decoration: none; }
a:active  { color: #9DCC00; }
a img { border: 0px; }
span.album_description { font-size: 0.9em; display: block; color: #404040; }
span.image_description { font-size: 0.9em; display: block; color: #404040; }
span.copy { display:block; float:left;}
#albums_tbl {width:100%;text-align:center;line-height:1.4em;}
#images_tbl {width:100%;text-align:center;line-height:1.4em;}
#view_tbl { width: 100%; text-align:center;}
#view_tbl td { line-height: 1.4em; color: #808080; }
#nav { padding:7px;color:#D0D0D0;background-color:#808080; margin: 0px 1px 0px 1px; line-height:1.4em; }
#sort { padding:0px 0px 7px 7px;color:#D0D0D0;background-color:#808080; margin: 0px 1px 0px 1px; line-height:1.4em; }
#nav a { color: white;font-weight:bold; }
#sort a { color: #d0d0d0; }
#pages { text-align:right;padding:5px; background-color:#9A9A9A;margin:0px 1px 0px 1px;color:#CFCFCF;}
#pages a { color: white; }
#description { background-color: #A9A9A9; margin: 0px 1px 0px 1px; padding: 8px; color:#F0F0F0; }
#sort { font-weight: normal; }
</style>
<script type="text/javascript">
<!--
function alternateRowColor ( id, el, color1, color2 ) { var obj = document.getElementById(id); if ( obj ) { var els = obj.getElementsByTagName ( el ); for ( var i = 0; i < els.length; i++ ) if ( !els[i].getAttribute ( 'skip_alternate' ) ) els[i].style.backgroundColor = ( i & 1 ? color1 : color2 ); }}
function alternateIt ( )
{
    alternateRowColor ( 'albums_tbl','td','#ffffff','#fbfbfb');
    alternateRowColor ( 'images_tbl','td','#ffffff','#fbfbfb');
    alternateRowColor ( 'view_tbl','td','#ffffff','#fbfbfb');
}
function togView ( id ){var el = document.getElementById ( id ); if ( el ) el.style.display = el.style.display == 'none' ? 'block' : 'none';}
-->
</script>
</head>
<body onload="alternateIt();">

<div style="width: 630px; margin-left: auto; margin-right: auto;">
    <div style="padding: 8px;">
	<p id="nav">{navigation}</p>
	{if browse_albums}
	    <p id="sort">Sort images by: {name_sort}, {size_sort}, {modified_sort} &nbsp; &nbsp; &nbsp;Sort albums by: {album_images_sort}, {album_albums_sort}</p>
	    {if there_is_album_description}<p id="description"><strong>Album description:</strong> {show_album_description}</p>{endif there_is_album_description}
	    {if there_are_albums}{show_albums}{endif there_are_albums}
	    {if there_are_images}{show_images}{endif there_are_images}
	    <p id="pages"><span class="copy">Simple Gallery 2.2 &copy; www.celerondude.com</span>&nbsp;{if there_are_pages}Go to page: {show_pages}{endif there_are_pages}</p>
	{endif browse_albums}
	{if view_image}
	</p>
	<table id="view_tbl" cellspacing="1" cellpadding="8">
	    <tr>
		{if there_is_a_previous_image}<td style="vertical-align:top;">{show_previous}</td>{endif there_is_a_previous_image}
		{if current_image_exists}<td style="vertical-align:top;">{show_current}</td>{endif current_image_exists}
		{if there_is_a_next_image}<td style="vertical-align:top;">{show_next}</td>{endif there_is_a_next_image}
	    </tr>
	</table>
	<p id="pages"><span class="copy">Simple Gallery 2.2 &copy; www.celerondude.com</span>&nbsp;</p>
	{endif view_image}
    </div>
</div>
</body>
</html>
EOT;
?>
