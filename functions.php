<?php
function dir_info ( $dir )
{
    $dir = $dir != '' && $dir != '/' ? trim ( $dir, '/' ) . '/' : '';
    $h = @opendir ( $dir );
    if ( !$h )
    {
        exit ( 'Unable to open ' . $dir );
    }
    $count['dirs'] = 0;
    $count['files'] = 0;
    $count['size'] = 0;
    while ( false != ( $f = readdir ( $h ) ) )
    {
        if ( $f != '.' && $f != '..' )
        {
            if ( is_dir ( $dir . $f ) )
            {
                $count['dirs']++;
            }
            else
            {
                $count['files']++;
                $count['size'] += filesize ( $dir . $f );
            }
        }
    }
    return $count;
}


function dir_get_contents ( $root, $dir )
{
    $dir = $dir != '' && $dir != '/' ? trim ( $dir, '/' ) : '';
    $path = $root . $dir . '/';
    $h = opendir ( $path );
    if ( !$h )
    {
        exit ( 'Unable to open ' . $path );
    }

    $contents['images'] = array( );

    $contents['albums'] = array ( );

    while ( false != ( $f = readdir ( $h ) ) )
    {
        if ( $f != '.' && $f != '..' )
        {
            if ( is_dir ( $path . $f ) )
            {
                $info = dir_info ( $path . $f );

                $contents['albums'][] = array ( 'name' => $f, 'albums' => $info['dirs'], 'images' => $info['files'] );
            }
            elseif ( preg_match ( '#.jpg$#i', $f ) )
            {
                $info = getimagesize ( $path . $f );
                $dimension = $info[0] . 'x' . $info[1];
                $contents['images'][] = array ( 'name' => $f, 'dimension' => $dimension, 'modified' => filemtime ( $path . $f ), 'size' => filesize ( $path . $f ) );
            }
        }
    }
    return $contents;
}


function create_table ( $cell_values, $columns = 4, $table_start = '<table>', $tr_start = '<tr>', $td_start = '<td>', $empty_cell = '<div style="clear:both;float:left;"></div>' )
{
    $i = 0;
    $rows = ceil ( count ( $cell_values ) / $columns );
    $html[] = $table_start;
    for ( $r = 0; $r < $rows; $r++ )
    {
        $html[] = '  ' . $tr_start;
        for ( $c = 0; $c < $columns; $c++ )
        {
            $html[] = '    ' . $td_start . ( isset ( $cell_values[$i] ) ? $cell_values[$i] : $empty_cell ) . '</td>';
            $i++;
        }
        $html[] = '  </tr>';
    }
    $html[] = '</table>';
    return implode ( "\n", $html );
}

function read ( $file )
{
    $fp = @fopen ( $file, 'rb' );
    if ( $fp )
    {
        if ( filesize ( $file ) === 0 )
        {
            return array ( );
        }
        flock ( $fp, 1 );
        return unserialize ( fread ( $fp, filesize ( $file ) ) );
    }
    else
    {
        exit ("Could not open $file for reading.");
    }
}

function write ( $file, $data )
{
    $fp = @fopen ( $file, 'wb' );
    if ( $fp )
    {
        flock ( $fp, 2 );
        fwrite ( $fp, serialize ( $data ) );
        fclose ( $fp );
    }
    else
    {
        exit ("Could not open $file for writting.");
    }
}

function slice ( $str, $width, $pos = 1 )
{
    if ( strlen ( $str ) <= $width )
    {
        return $str;
    }
    if ( $pos === 0 )
    {
        return '...' . substr ( $str, 3, $width - 3 );
    }
    if ( $pos === 1 )
    {
        return substr ( $str, 0, ( $width / 2 ) - 1 ) . '...' . substr ( $str, - ( $width / 2 - 2 ) );
    }
    else
    {
        return substr ( $str, 0, $width - 3 ) . '...';
    }
}

function parse ( &$template, $data, $x = '' )
{
    if ( !is_array ( $data ) )
    {
        return str_replace ($data, $x, $template);
    }
    else
    {
        return str_replace ( array_keys($data), array_values($data), $template );
    }
}

function timer ($start = 0, $digits = 8)
{
    list ($m, $s) = explode(' ', microtime( ) );
    return round ( (double)$s + (double)$m - $start, $digits);
}

function parse_if ( &$template, $varname, $varval, $false_replace = '' )
{
    $$varname = $varval;
    return preg_replace ( '#{if ' . $varname . '}(.+?){endif ' . $varname .'}#ise', '($' . $varname . ')?"$1":"$false_replace"', $template );
}

function make_dir ( $root, $dir, $mode = 0777 )
{
    $o = umask ( 0 );
    $dir = trim ( $dir, '/' );
    $paths = explode ( '/', $dir );
    $gone = array ( );
    for ( $i = 0; $i < count ( $paths ); $i++ )
    {
        $gone[] = $paths[$i];
        $d = $root . implode ( '/', $gone );
        if ( !is_dir ( $d ) )
        {
            mkdir ( $root . implode ( '/', $gone ), $mode );
        }
    }
    umask ( $o );
}

function multisort ($array, $column, $order = 'asc', $type = SORT_STRING)
{
    if (!isset ($array[0][$column]) )
    {
        return $array;
    }
    for ($i = 0; $i < count ($array); $i++)
    {
        $temp[$i] = $array[$i][$column];
    }
    switch ($order)
    {
        default:
        case 'asc':
            $order = SORT_ASC;
        break;
        case 'dsc':
        case 'desc':
            $order = SORT_DESC;
        break;
    }
    array_multisort ($temp, $order, $type , $array);

    return $array;
}


function create_thumbnail ($source, $destination, $new_width = 100, $new_height = 'auto', $quality = 85)
{
    $im_src = imagecreatefromjpeg($source);
    if (!$im_src) return;
    $im_width = imagesX($im_src);
    $im_height = imagesY($im_src);
    if( !is_int($new_width) && is_int($new_height) )
    {
        // resize the image based on height
        $ratio = $im_height / $new_height;
        $new_width = floor($im_width / $ratio);
    }
    elseif( is_int($new_width) && !is_int($new_height) )
    {
        // resize the image based on the width
        $ratio = $im_width / $new_width;
        $new_height = floor($im_height / $ratio);
    }
    // create blank image
    $im = imagecreatetruecolor($new_width, $new_height);
    imagecopyresampled($im, $im_src, 0, 0, 0, 0, $new_width, $new_height, $im_width, $im_height);
    imagejpeg($im, $destination, $quality);
    imagedestroy($im);
    imagedestroy($im_src);
}
?>
