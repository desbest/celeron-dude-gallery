<?php
function gallery ( )
{
    #---------------------------------------------
    # Configurations. Edit in this section!
    #---------------------------------------------
    $conf_root = './';  // the path to where the gallery files are located
    $conf_root_url = './'; // the url that points to the path above.
    $conf_gallery_url = 'index.php?'; // the url that you would type into the address bar to access the gallery. note the ?. If you use something like index.php?page=gallery, then add &amp; at the end. Otherwise just leave the question mark ?

    $conf_style = 'style_new.php'; // name of the style file you want to use
    $conf_cache = 0;  // cache file list, recommended but you must clear the cache folder everytime you update your albums
    $conf_cache_timeout = 60; // how long before the cache expires. Setting this to 0 means never.
    $conf_create_thumbnails = 1; // create thumbnails? if not, a generic icon will be used
    $conf_images_per_page = 12; // how many images perpage?
    $conf_use_large_thumbnails = 1; // setting this to 1 will not create a larger thumbnail. The smaller thumbnail will be used instead.
    $conf_small_thumbnails_width = 100; // the width of the small thumbnails
    $conf_large_thumbnails_width = 200; // the width of the large thumbnails
    $conf_default_sort_by = 'name'; // name/size/modifed (also means uploaded )
    $conf_default_sort_order = 'asc'; // asc or dsc


    # Run startup checking for paths and stuff
    if ( !is_dir ( $conf_root ) ) { print 'Could not open the root directory.'; return; }
    if ( !is_dir ( $conf_root . 'cache' ) ) { print 'Could not open the cache directory.'; return; }
    if ( !is_dir ( $conf_root . 'albums' ) ) { print 'Could not open the albums directory.'; return; }
    if ( !is_dir ( $conf_root . 'thumbnails' ) ) { print 'Could not open the thumbnails directory.'; return; }

    if ( !is_writeable ( $conf_root . 'cache' ) ) { print 'The cache directory does not have write permission.'; return; }
    if ( !is_writeable ( $conf_root . 'thumbnails' ) ) { print 'The thumbnails directory does not have write permission.'; return; }
    if ( !function_exists ( 'imagecreatetruecolor' ) ) { print 'There is no such function "imagecreatetruecolor" It seems you do not have GD installed.'; return;}

    # Load files
    require_once ( $conf_root . 'functions.php' );
    require_once ( $conf_root . $conf_style );

    //$debug = timer ( );


    # Clean GET
    if ( get_magic_quotes_gpc ( ) )
    {
        while ( list ( $x, $y ) = each ( $_GET ) )
        {
            $_GET [ $x ] = stripslashes ( $y );
        }
    }

    # Get necessary variables from the URL
    $album = isset ( $_GET['album'] ) ? trim ( urldecode ( $_GET['album'] ), '/' ) : '';
    if ( $album !== '' ) { $album .= '/'; }
    if ( strstr ( $album, '../' ) ) exit ( 'Denied' );
    $action = isset ( $_GET['action'] ) ? $_GET['action'] : 'browse';
    $page = isset ( $_GET['page'] ) ? abs ( intval ( $_GET['page'] ) ) : 1;
    $image = isset ( $_GET['image'] ) ? abs ( intval ( $_GET['image'] ) ) : 1;
    $sortby = isset ( $_GET['sortby'] ) ? $_GET['sortby'] : $conf_default_sort_by;
    $order = isset ( $_GET['order'] ) ? $_GET['order'] : $conf_default_sort_order;

    #--------------------------------------------------
    # Load images list from cache or create a new list
    #--------------------------------------------------
    $cache_file = $conf_root . 'cache/' . ( $album !== '' ? str_replace ( '/', '_', trim ( $album, '/' ) ) : '_' ) . '.txt';

    if ( $conf_cache )
    {
        if ( file_exists ( $cache_file ) )
        {
            if ( $conf_cache_timeout )
            {
                if ( time ( ) - filemtime ( $cache_file ) > $conf_cache_timeout )
                {
                    $contents = dir_get_contents ( $conf_root . 'albums/', $album );
                    write ( $cache_file, $contents );
                }
                else
                {
                    $contents = read ( $cache_file );
                }
            }
            else
            {
                $contents = read ( $cache_file );
            }
        }
        else
        {
            $contents = dir_get_contents ( $conf_root . 'albums/', $album );
            write ( $cache_file, $contents );
        }
    }
    else
    {
        $contents = dir_get_contents ( $conf_root . 'albums/', $album );
    }

    $albums =& $contents['albums'];
    $images =& $contents['images'];
    $albums_count = count ( $albums );
    $images_count = count ( $images );
    if ( $image === 0 )
    {
        $image = 1;
    }
    if ( $image > $images_count )
    {
        $image = $images_count;
    }

    # Take care of sorting, create sort link

    $type = $sortby == 'name' ? SORT_STRING : SORT_NUMERIC;
    $images = multisort ( $images, $sortby, $order, $type );
    $albums = multisort ( $albums, $sortby, $order, $type );

    if ( $action === 'browse' )
    {
        $sort_links['{name_sort}'] = '<a href="' . $conf_gallery_url . 'album=' . rawurlencode ( trim ( $album, '/' ) ) . '&amp;sortby=name&amp;order=' . ( $order == 'asc' ? 'dsc' : 'asc' ) . '">Name</a>';
        $sort_links['{size_sort}'] = '<a href="' . $conf_gallery_url . 'album=' . rawurlencode ( trim ( $album, '/' ) ) . '&amp;sortby=size&amp;order=' . ( $order == 'asc' ? 'dsc' : 'asc' ) . '">Size</a>';
        $sort_links['{modified_sort}'] = '<a href="' . $conf_gallery_url . 'album=' . rawurlencode ( trim ( $album, '/' ) ) . '&amp;sortby=modified&amp;order=' . ( $order == 'asc' ? 'dsc' : 'asc' ) . '">Modified</a>';
        $sort_links['{album_albums_sort}'] = '<a href="' . $conf_gallery_url . 'album=' . rawurlencode ( trim ( $album, '/' ) ) . '&amp;sortby=albums&amp;order=' . ( $order == 'asc' ? 'dsc' : 'asc' ) . '">Sub albums</a>';
        $sort_links['{album_images_sort}'] = '<a href="' . $conf_gallery_url . 'album=' . rawurlencode ( trim ( $album, '/' ) ) . '&amp;sortby=images&amp;order=' . ( $order == 'asc' ? 'dsc' : 'asc' ) . '">Albums contents</a>';
        $style_gallery = parse ( $style_gallery, $sort_links );
    }


    # Build navigation
    if ( $album == '' )
    {
        $navigation = ( $action == 'view' ? '<a href="' . $conf_gallery_url . '&amp;sortby=' . $sortby . '&amp;order=' . $order . '">' . $style_gallery_title . '</a>' : $style_gallery_title );
    }
    else
    {
        $paths = explode ( '/', trim ( $album, '/' ) );
        $navigation [] = '<a href="' . $conf_gallery_url . '&amp;sortby=' . $sortby . '&amp;order=' . $order . '">' . $style_gallery_title . '</a>';
        if ( count ( $paths ) === 1 )
        {
            if ( $action === 'view' )
            {
                $navigation [] = '<a href="' . $conf_gallery_url . 'album=' . rawurlencode ( $paths[0] ) . '&amp;sortby=' . $sortby . '&amp;order=' . $order . '">' . $paths[0] . '</a>';
                $current = isset ( $images[ $image - 1] ) ? $images[ $image - 1] : false;
                if ( $current )
                {
                    $navigation [] = $current['name'];
                }
                else
                {
                    $navigation [] = 'Invalid image';
                }
            }
            else
            {
                $navigation [] = $paths[0];
            }
        }
        else
        {
            $gone = array ();
            $x = ( $action === 'view' ) ? 0 : 1;
            for ( $i = 0; $i < count ( $paths ) - $x; $i++ )
            {
                $gone[] = $paths[$i];
                $navigation [] = '<a href="' . $conf_gallery_url . 'album=' . rawurlencode ( implode ( '/', $gone ) ) . '&amp;sortby=' . $sortby . '&amp;order=' . $order . '">' . $paths[$i] . '</a>';
            }

            if ( $action === 'view' )
            {
                $current = isset ( $images[ $image - 1] ) ? $images[ $image - 1] : false;
                if ( $current )
                {
                    $navigation [] =  $current['name'];
                }
                else
                {
                    $navigation [] = 'Invalid image';
                }
            }
            else
            {
                $navigation [] = end ( $paths );
            }
        }
        $navigation = implode ( $style_navigation_bullet, $navigation );
    }







    #--------------------------------------------------
    # run based on action
    #--------------------------------------------------
    if ( $action === 'view' )
    {
        if ( !$images_count )
        {
            print 'This album has no images.';
            return;
        }

        $output = array ( );

        // previous
        if ( $image > 1 )
        {
            $previous = $images[ $image - 2 ];

            $thumbnail = file_exists ( $conf_root . 'thumbnails/' . $album . 'SMALL_' . $previous['name'] ) ?  '<img src="' . $conf_root_url . 'thumbnails/' . $album . 'SMALL_' . $previous['name'] . '" alt="' . $previous['name'] . '" />' :  '<img src="' . $conf_root_url . 'images/images.gif" alt="' . $previous['name'] . '" />';

            $previous_image = '<a href="' . $conf_gallery_url . 'action=view&amp;album=' . rawurlencode ( trim ( $album, '/' ) ) . '&amp;image=' . ( $image - 1 ) . '&amp;sortby=' . $sortby . '&amp;order=' . $order .'">' . $thumbnail . '</a><br />' . slice ( $previous['name'], $style_name_slice ) . '<br />&laquo; Previous';
        }
        else
        {
            $previous_image = '';
        }
        // current
        if ( isset ( $images[ $image - 1] ) )
        {
            $img =& $images[ $image - 1];

            if ( $conf_use_large_thumbnails )
            {
                $thumbnail = file_exists ( $conf_root . 'thumbnails/' . $album . 'LARGE_' . $img['name'] ) ?  '<img src="' . $conf_root_url . 'thumbnails/' . $album . 'LARGE_' . $img['name'] . '" alt="' . $img['name'] . '" />' :  '<img src="' . $conf_root_url . 'images/images.gif" alt="' . $img['name'] . '" />';
            }
            else
            {
                $thumbnail = file_exists ( $conf_root . 'thumbnails/' . $album . 'SMALL_' . $img['name'] ) ?  '<img src="' . $conf_root_url . 'thumbnails/' . $album . 'SMALL_' . $img['name'] . '" alt="' . $img['name'] . '" />' :  '<img src="' . $conf_root_url . 'images/images.gif" alt="' . $img['name'] . '" />';
            }

            $current_image = '<a href="' . $conf_root_url . 'albums/' . $album . $img['name'] . '">' . $thumbnail . '</a><br />Click to enlarge<br />' . '<br />Image ' . $image . ' of ' . $images_count . '<br />' . $img['name'] . '<br />Dimension: ' . $img['dimension'] . '<br />Filesize: ' . ( $img['size'] / 1000 ). 'KB';
        }
        else
        {
            $current_image = '';
        }

        // next
        if ( $image < $images_count )
        {
            $next = $images[ $image ];

            $thumbnail = file_exists ( $conf_root . 'thumbnails/' . $album . 'SMALL_' . $next['name'] ) ?  '<img src="' . $conf_root_url . 'thumbnails/' . $album . 'SMALL_' . $next['name'] . '" alt="' . $next['name'] . '" />' :  '<img src="' . $conf_root_url . 'images/images.gif" alt="' . $next['name'] . '" />';

            $next_image = '<a href="' . $conf_gallery_url . 'action=view&amp;album=' . rawurlencode ( trim ( $album, '/' ) ) . '&amp;image=' . ( $image + 1 ) . '&amp;sortby=' . $sortby . '&amp;order=' . $order .'">' . $thumbnail . '</a><br />' . slice ( $next['name'], $style_name_slice ) . '<br />Next &raquo;';
        }
        else
        {
            $next_image = '';
        }

        $style_gallery = parse_if ( $style_gallery, 'there_is_a_previous_image', ( $previous_image !== '' ) );
        $style_gallery = parse_if ( $style_gallery, 'there_is_a_next_image', ( $next_image !== '' ) );
        $style_gallery = parse_if ( $style_gallery, 'current_image_exists', ( $current_image !== '' ), 'Invalid image' );

        $style_gallery = parse_if ( $style_gallery, 'browse_albums', 0 );
        $style_gallery = parse_if ( $style_gallery, 'view_image', 1 );
        print parse ( $style_gallery, array ( '{navigation}' => $navigation, '{show_previous}' => $previous_image, '{show_current}' => $current_image, '{show_next}' => $next_image ) );
    }
    else
    {
        #--------------------------------------------------
        # Show any album description
        #--------------------------------------------------
        $desc_file = $conf_root . 'albums/' . $album . 'description.txt';

        if ( $action !== 'view' && file_exists ( $desc_file ) )
        {
            $fp = fopen ( $desc_file, 'r' );
            $description = fread ( $fp, filesize ( $desc_file ) );
            fclose ( $fp );
        }
        else
        {
            $description = '';
        }
        #--------------------------------------------------
        # Show albums
        #--------------------------------------------------
        if ( $albums_count )
        {
            $output = array ( );

            while ( list ( $i, ) = each ( $albums ) )
            {
                $album_url = $conf_gallery_url . 'album=' . rawurlencode ( $album . $albums[$i]['name'] ) . '&amp;sortby=' . $sortby . '&amp;order=' . $order;
                $output [] = '<a href="'.$album_url.'"><img src="' . $conf_root_url . 'images/folder.gif" alt="' . $albums[$i]['name'] . '" /></a><br /><a href="'.$album_url.'">' . slice ( $albums[$i]['name'], $style_name_slice ) . '</a><span class="album_description">' . ( $albums[$i]['albums'] ? $albums[$i]['albums'] . ' album' .($albums[$i]['albums']>1?'s':'') : '' ) . ( $albums[$i]['images'] && $albums[$i]['albums'] ? ' - ' : '' ) .  ( $albums[$i]['images'] ? $albums[$i]['images'] . ' image'. ($albums[$i]['images']>1?'s':'') : '' ) . '</span>';
            }

            $albums_table = create_table ( $output, ( $albums_count < $style_albums_columns ? $albums_count : $style_albums_columns ), $style_albums_start, $style_albums_rows, $style_albums_cols );
        }
        else
        {
            $albums_table = '';
        }


        #--------------------------------------------------
        # Show images
        #--------------------------------------------------
        if ( $images_count )
        {
            // devide images into pages
            if ( $conf_images_per_page > 0 )
            {
                if ( $images_count < $conf_images_per_page )
                {
                    $current_page = 1;
                    $pages = 1;
                }
                else
                {
                    $current_page = $page;
                    $pages = ceil ( $images_count / $conf_images_per_page );
                }
                if ( $page < 1 )
                {
                    $page = 1;
                    $current_page = $page;
                }
                elseif ( $page > $pages )
                {
                    $page = $pages;
                    $current_page = $page;
                }

                $chunks = array_chunk ( $images, $conf_images_per_page, 1 );

                $images = $chunks [ $page - 1 ];
            }
            else
            {
                $pages = 1;
                $current_page = 1;
            }

            $output = array ( );

            while ( list ( $i, ) = each ( $images ) )
            {
                if ( $conf_create_thumbnails )
                {
                    @set_time_limit ( 0 );

                    $source = $conf_root . 'albums/' . $album . $images[$i]['name'];

                    $small = $conf_root . 'thumbnails/' . $album . 'SMALL_' . $images[$i]['name'];

                    $large = $conf_root . 'thumbnails/' . $album . 'LARGE_' . $images[$i]['name'];

                    if ( !is_dir ( $conf_root . 'thumbnails/' . $album ) )
                    {
                        make_dir ( $conf_root . 'thumbnails/', $album );
                    }
                    if ( !file_exists ( $small ) )
                    {
                        create_thumbnail ( $source, $small, $conf_small_thumbnails_width, 'auto',  90 );
                    }
                    if ( $conf_use_large_thumbnails && !file_exists ( $large ) )
                    {
                        create_thumbnail ( $source, $large, $conf_large_thumbnails_width, 'auto',  85 );
                    }
                }
                $thumbnail = file_exists ( $conf_root . 'thumbnails/' . $album . 'SMALL_' . $images[$i]['name'] ) ?  '<img src="' . $conf_root_url . 'thumbnails/' . $album . 'SMALL_' . $images[$i]['name'] . '" alt="' . $images[$i]['name'] . '" />' :  '<img src="' . $conf_root_url . 'images/images.gif" alt="' . $images[$i]['name'] . '" />';
                clearstatcache ( );
                $view_url = $conf_gallery_url . 'action=view&amp;album=' . rawurlencode ( trim ( $album, '/' ) ) . '&amp;image=' . ( $i + 1 ) . '&amp;sortby=' . $sortby . '&amp;order=' . $order;
                $output [] = '<a href="'.$view_url.'">' . $thumbnail . '</a><br /><a href="'.$view_url.'">'.slice ( $images[$i]['name'], $style_name_slice ) . '</a>' . '<span class="image_description">' . ( $images[$i]['size'] / 1000 ) . 'KB</span>';
            }

            $images_table = create_table ( $output, ( $images_count < $style_images_columns ? $images_count : $style_images_columns ), $style_images_start, $style_images_rows, $style_images_cols, '<div style="float:left;clear:both;"></div>' );
        }
        else
        {
            $images_table = '';
        }

        if ( $images_table === '' && $albums_table === '' )
        {
            $description = 'This album is empty.';
        }
        #--------------------------------------------------
        # Show pages
        #--------------------------------------------------
       if ( $action == 'browse' && isset ( $pages ) && $pages > 1 )
       {
           $pages_links = array();

           if ( $pages > 1 )
           {
               if ( $current_page > 1 )
               {
                   $pages_links []= '<a href="' . $conf_gallery_url . 'album=' . rawurlencode ( trim ( $album, '/' ) ) . '&amp;page=' . ( $current_page - 1 ) . '&amp;sortby=' . $sortby . '&amp;order=' . $order . '">Previous</a>';
               }
               else
               {
                   $pages_links []= '<a href="' . $conf_gallery_url . 'album=' . rawurlencode ( trim ( $album, '/' ) ) . '&amp;page=' . $pages . '&amp;sortby=' . $sortby . '&amp;order=' . $order . '">Last page</a>';
               }
           }

           for ( $i = 1; $i <= $pages; $i++ )
           {
               $pages_links []= ( $i == $current_page ? '<span style="text-decoration:underline;font-weight:bold;">' .$i . '</span>' : '<a href="' . $conf_gallery_url . 'album=' . rawurlencode ( trim ( $album, '/' ) ) . '&amp;page=' . ( $i ) . '&amp;sortby=' . $sortby . '&amp;order=' . $order . '">' . $i . '</a>' );
           }

           if ( $pages > 1 )
           {
               if ( $current_page < $pages )
               {
                   $pages_links []= '<a href="' . $conf_gallery_url . 'album=' . rawurlencode ( trim ( $album, '/' ) ) . '&amp;page=' . ( $current_page + 1 ) . '&amp;sortby=' . $sortby . '&amp;order=' . $order . '">Next</a>';
               }
               else
               {
                   $pages_links []= '<a href="' . $conf_gallery_url . 'album=' . rawurlencode ( trim ( $album, '/' ) ) . '&amp;page=1' . '&amp;sortby=' . $sortby . '&amp;order=' . $order . '">First page</a>';
               }
           }
           $pages_links = implode ( '&nbsp; ', $pages_links );
       }
       else
       {
           $pages_links = '';
       }

        #--------------------------------------------------
        # Print
        #--------------------------------------------------
        $style_gallery = parse_if ( $style_gallery, 'there_is_album_description', $description != '' );
        $style_gallery = parse_if ( $style_gallery, 'there_are_pages', $pages_links !== '' );
        $style_gallery = parse_if ( $style_gallery, 'browse_albums', 1 );
        $style_gallery = parse_if ( $style_gallery, 'view_image', 0 );
        $style_gallery = parse_if ( $style_gallery, 'there_are_albums', $albums_count );
        $style_gallery = parse_if ( $style_gallery, 'there_are_images', $images_count );
        print parse ( $style_gallery, array ( '{navigation}' => $navigation, '{show_albums}' => $albums_table, '{show_images}' => $images_table, '{show_pages}' => $pages_links, '{show_album_description}' => $description ) );
    }

    //print timer ( $debug );

}

gallery ( );
?>
