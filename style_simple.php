<?php
// This style should be used if you are including the gallery into your existing layout.
// It will inherit most of the styles from your layout, but you should also add
// some of the gallery css styles (from style_new.php or style_old.php) to your existing css style.

$style_gallery_title = 'Simple Gallery';
$style_navigation_bullet = ' <span style="font-size: 10px;font-family:verdana;">/</span> ';
$style_name_slice = 25; // cut long names to 30 characters

$style_albums_start = '<table id="albums_tbl" cellspacing="1" cellpadding="8">';
$style_albums_rows = '<tr>';
$style_albums_cols = '<td style="width:25%">';
$style_albums_columns = 4;

$style_images_start = '<table id="images_tbl" cellspacing="1" cellpadding="8">';
$style_images_rows = '<tr>';
$style_images_cols = '<td style="width:25%">';
$style_images_columns = 4;


$style_gallery = <<<EOT
<div style="width: 630px; margin-left: auto; margin-right: auto;">
    <div style="padding: 8px;">
	<p id="nav">{navigation}</p>
	{if browse_albums}
	    <p id="sort">Sort images by: {name_sort}, {size_sort}, {modified_sort} &nbsp; &nbsp; &nbsp;Sort albums by: {album_images_sort}, {album_albums_sort}</p>
	    {if there_is_album_description}<p id="description"><strong>Album description:</strong> {show_album_description}</p>{endif there_is_album_description}
	    {if there_are_albums}{show_albums}{endif there_are_albums}
	    {if there_are_images}{show_images}{endif there_are_images}
	    <p id="pages"><span class="copy">Simple Gallery 2.2 &copy; www.celerondude.com</span>&nbsp;{if there_are_pages}Go to page: {show_pages}{endif there_are_pages}</p>
	{endif browse_albums}
	{if view_image}
	</p>
	<table id="view_tbl" cellspacing="1" cellpadding="8">
	    <tr>
		{if there_is_a_previous_image}<td style="vertical-align:top;">{show_previous}</td>{endif there_is_a_previous_image}
		{if current_image_exists}<td style="vertical-align:top;">{show_current}</td>{endif current_image_exists}
		{if there_is_a_next_image}<td style="vertical-align:top;">{show_next}</td>{endif there_is_a_next_image}
	    </tr>
	</table>
	<p id="pages"><span class="copy">Simple Gallery 2.2 &copy; www.celerondude.com</span>&nbsp;</p>
	{endif view_image}
    </div>
</div>
EOT;
?>
